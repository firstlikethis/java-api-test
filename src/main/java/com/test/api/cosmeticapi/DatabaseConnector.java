package com.test.api.cosmeticapi;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DatabaseConnector {
    public List<Cosmetic> getAllCosmetics() {
        Connection conn = null;
        List<Cosmetic> cosmetics = new ArrayList<>();
        try {
            String dbUrl = "jdbc:mysql://localhost:3306/cosmetics?useSSL=false&serverTimezone=UTC";
            String dbUsername = "root";
            String dbPassword = "";

            conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
            String query = "SELECT * FROM cosmetics";
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                Cosmetic cosmetic = new Cosmetic(rs.getString("id"), rs.getString("name"), rs.getInt("amount"));
                cosmetics.add(cosmetic);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
        return cosmetics;
    }

    public void deleteCosmetics(String id) {
        Connection conn = null;
        try {
            String dbUrl = "jdbc:mysql://localhost:3306/cosmetics?useSSL=false&serverTimezone=UTC";
            String dbUsername = "root";
            String dbPassword = "";

            conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
            String query = "DELETE FROM cosmetics WHERE id=?";
            PreparedStatement stmt = conn.prepareStatement(query);
            stmt.setString(1, id);
            stmt.executeUpdate();
        } catch (SQLException ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void addCosmetics(String id, String name, int amount) {
        Connection conn = null;
        try {
            String dbUrl = "jdbc:mysql://localhost:3306/cosmetics?useSSL=false&serverTimezone=UTC";
            String dbUsername = "root";
            String dbPassword = "";

            conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
            String sql = "INSERT INTO cosmetics (id,name,amount) VALUES (?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, id);
            stmt.setString(2, name);
            stmt.setInt(3, amount);
            stmt.execute();
        } catch (Exception ea) {
            ea.printStackTrace();
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ea) {
                ea.printStackTrace();
            }
        }
    }


    public Cosmetic searchCosmetics(String id) {
        Connection conn = null;
        try {
            String dbUrl = "jdbc:mysql://localhost:3306/cosmetics?useSSL=false&serverTimezone=UTC";
            String dbUsername = "root";
            String dbPassword = "";
    
            conn = DriverManager.getConnection(dbUrl, dbUsername, dbPassword);
            String sql = "SELECT * FROM cosmetics WHERE id=?";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, id);
            ResultSet rs = stmt.executeQuery();
            if (rs.next()) {
                Cosmetic cosmetic = new Cosmetic(rs.getString("id"), rs.getString("name"), rs.getInt("amount"));
                return cosmetic;
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
    
    
}
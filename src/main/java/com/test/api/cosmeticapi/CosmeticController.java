package com.test.api.cosmeticapi;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CosmeticController {

    private DatabaseConnector dbConnector = new DatabaseConnector();

    @RequestMapping("/cosmetics")
    public List<Cosmetic> getAllCosmetics() {
        return dbConnector.getAllCosmetics();
    }

    @RequestMapping(value = "/cosmetics/{id}", method = RequestMethod.DELETE)
    public void deleteCosmetics(@PathVariable String id) {
        dbConnector.deleteCosmetics(id);
    }

    @RequestMapping(value = "/cosmetics/add", method = RequestMethod.POST)
    public void addCosmetics(@RequestBody Cosmetic cosmetic) {
        dbConnector.addCosmetics(cosmetic.getId(), cosmetic.getName(), cosmetic.getAmount());
    }

    @RequestMapping(value = "/cosmetics/search/{id}", method = RequestMethod.GET)
    public ResponseEntity<Cosmetic> getCosmeticById(@PathVariable("id") String id) {
        Cosmetic cosmetic = dbConnector.searchCosmetics(id);
        if (cosmetic != null) {
            return new ResponseEntity<>(cosmetic, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
